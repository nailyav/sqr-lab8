import unittest
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By


class SubmitComment(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
    

    def test_searching_go_packages(self):
        browser = self.driver
        browser.get("https://winxclubfanwebsite.wordpress.com/music/")

        # Comment Input element
        message = 'I love winx club!!!'
        comment_input = browser.find_element('id', 'comment')
        comment_input.send_keys(message)

        # Email Input element
        email_input = browser.find_element('id', 'email')
        email_input.send_keys('fan@winxclub.com')

        # Name Input element
        name_input = browser.find_element('id', 'author')
        name_input.send_keys('humble winx fan')

        # Click to Submit the comment
        browser.find_element('name', 'submit').click()
 

if __name__ == "__main__":
    unittest.main()