# Testcases

## "Submit comment" works

| Step Number | What Done                                                        | Status | Comment                                                       |
|-------------|------------------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Navigate to https://winxclubfanwebsite.wordpress.com/music/      | Pass   | The page loaded successfully                                  |
| 2           | Verify the presence of the comment section                       | Pass   | Comment section is present                                    |
| 3           | Fill the comment section with random comment                     | Pass   | Inputs comment into the comment section                       |
| 4           | Verify the appearance of the email field                         | Pass   | Email field appeared after clicking on comment section        |
| 5           | Fill the email field with email                                  | Pass   | Inputs email into the email field                             |
| 6           | Verify the presence of the name field                            | Pass   | Name field appeared after clicking on comment section         |
| 7           | Fill the name field with author's name                           | Pass   | Inputs author's name into the name field                      |
| 8           | Verify the presence of the submit button                         | Pass   | Submit button is present                                      |
| 9           | Submit the comment                                               | Pass   | Comment is submitted                                          |

## "Searching "A" gives more than 0 results" link works

| Step Number | What Done                                              | Status | Comment                                                       |
|-------------|--------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Navigate to https://winxclubfanwebsite.wordpress.com/  | Pass   | The page loaded successfully                                  |
| 2           | Verify the presence of the search button               | Pass   | The search button is present                                  |
| 3           | Click on the search button                             | Pass   | Clicked on the search button                                  |
| 4           | Verify that input field is present                     | Pass   | The input field appeared after clicking on search button      |
| 5           | Type in "A"                                            | Pass   | Successfully filles the input field with "A" string           |
| 6           | Verify the presence of the search button               | Pass   | The search button is present                                  |
| 3           | Click on the search button                             | Pass   | Clicked on the search button                                  |
| 6           | Verify the presence of more than 0 results             | Pass   | More than 0 results are shown                                 |


## "Leave a reply" works

| Step Number | What Done                                                             | Status | Comment                                                  |   
|-------------|-----------------------------------------------------------------------|--------|----------------------------------------------------------|
| 1           | Navigate to https://winxclubfanwebsite.wordpress.com/transformations/ | Pass   | The page loaded successfully                             |
| 2           | Verify the presence of the reply section                              | Pass   | Reply section is present                                 |
| 3           | Fill the reply section with random reply                              | Pass   | Inputs reply into the reply section                      |
| 4           | Verify the appearance of the email field                              | Pass   | Email field appeared after clicking on reply section     |
| 5           | Fill the email field with email                                       | Pass   | Inputs email into the email field                        |
| 6           | Verify the presence of the name field                                 | Pass   | Name field appeared after clicking on reply section      |
| 7           | Fill the name field with author's name                                | Pass   | Inputs author's name into the name field                 |
| 8           | Verify the presence of the submit button                              | Pass   | Submit button is present                                 |
| 9           | Submit the reply                                                      | Pass   | Reply is submitted                                       |

